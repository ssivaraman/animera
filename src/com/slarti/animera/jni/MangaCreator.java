package com.slarti.animera.jni;

import android.content.res.AssetManager;
import android.graphics.Bitmap;

/**
 * Created with IntelliJ IDEA.
 * User: Sid
 * Date: 19/5/13
 * Time: 10:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class MangaCreator {

    static {
        System.loadLibrary("gnustl_shared");
        System.loadLibrary("manga_creator");
    }

    public static native void createManga(String input, String output, int orientation);

    public static native void addCaption(String imagePath, Bitmap captionBitmap);

    public static native void loadAssets(AssetManager assetManager);
}
