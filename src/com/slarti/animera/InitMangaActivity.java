package com.slarti.animera;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Sid
 * Date: 18/5/13
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class InitMangaActivity extends ActionBarActivity {
    public static final int CAPTURE_REQUEST_CODE = 99;
    public static final int GALLERY_LOAD_REQUEST_CODE = 100;
    public static final int BROWSE_REQUEST_CODE = 101;
    private static final String TAG = "InitMangaActivity";
    private static String imagePath;
    private long captureTime = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // View should show a graphic and button to take a picture at the center bottom
        setContentView(R.layout.main);

        if (savedInstanceState != null) {
            imagePath = savedInstanceState.getString("imagePath");
        }

        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();
    }

    // On button click go to CaptureMangaActivity
    public void onCreateFromCameraClicked(View v){
        // Start the camera app and let it take a picture
        Intent imageCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri fileUri = getOutputMediaFileUri();

        imagePath = fileUri.getPath();

        imageCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);

        startActivityForResult(imageCaptureIntent,CAPTURE_REQUEST_CODE);
    }

    public void onCreateFromGalleryClicked(View v){
        // Start the camera app and let it take a picture
        Intent galleryIntent = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // Here we don't actually create an input image at imagePath, but the created manga derives its path from here
        // So we set the imagePath
        Uri fileUri = getOutputMediaFileUri();
        imagePath = fileUri.getPath();

        startActivityForResult(galleryIntent, GALLERY_LOAD_REQUEST_CODE);
    }

    // ON return from camera app get the path of the newly formed image
    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == CAPTURE_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                String mangaPath = getOutputPath(imagePath);
                startEditMangaActivity(imagePath,mangaPath,false);
            }
        } else if ((requestCode == GALLERY_LOAD_REQUEST_CODE) && (resultCode == RESULT_OK)) {
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImageUri,filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            String mangaPath = getOutputPath(imagePath);

            // String picturePath contains the path of selected Image
            startEditMangaActivity(picturePath,mangaPath,true);
        } else {

        }
    }

    private void startEditMangaActivity(String path, String mangaPath,boolean fromGallery){
        // Get the exif orientation properly from the image
        int orientation = getExifOrientationFromImage(path);

        Intent editIntent = new Intent(this,EditMangaActivity.class);
        editIntent.putExtra("imagePath",path);
        editIntent.putExtra("mangaPath",mangaPath);
        editIntent.putExtra("orientation",orientation);
        editIntent.putExtra("fromGallery",fromGallery);

        startActivity(editIntent);
    }

    private int getExifOrientationFromImage(String filePath){
        int rotation = -1;
        long fileSize = new File(filePath).length();

        Cursor mediaCursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.ImageColumns.ORIENTATION, MediaStore.MediaColumns.SIZE}, MediaStore.MediaColumns.DATE_ADDED + ">=?", new String[]{String.valueOf(captureTime / 1000 - 1)}, MediaStore.MediaColumns.DATE_ADDED + " desc");

        if (mediaCursor != null && captureTime != 0 && mediaCursor.getCount() !=0 ) {
            while(mediaCursor.moveToNext()){
                long size = mediaCursor.getLong(1);
                //Extra check to make sure that we are getting the orientation from the proper file
                if(size == fileSize){
                    rotation = mediaCursor.getInt(0);
                    break;
                }
            }
        }

        if (rotation == -1){
            try {
                ExifInterface exif = new ExifInterface(filePath);     //Since API Level 5
                rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,0);
                if (rotation == ExifInterface.ORIENTATION_NORMAL){
                    rotation = 0;
                }else if (rotation == ExifInterface.ORIENTATION_ROTATE_90){
                    rotation = 90;
                } else if (rotation == ExifInterface.ORIENTATION_ROTATE_180) {
                    rotation = 180;
                } else if (rotation == ExifInterface.ORIENTATION_ROTATE_270) {
                    rotation = 270;
                }
            } catch (IOException ioe){
                ioe.printStackTrace();
            }
        }

        return rotation;
    }

    public Uri getOutputMediaFileUri(){
        File animeraDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                        "animera");

        if (!animeraDirectory.exists()){
            if (!animeraDirectory.mkdirs()){
                return null;
            }
        }

        // Name the file
        String timestamp = new SimpleDateFormat("yyMMdd-HHmmss").format(new Date());
        File animeraFile = new File(animeraDirectory.getPath(),timestamp + ".jpg");
        Uri animeraUri = Uri.fromFile(animeraFile);

        return animeraUri;
    }

    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        bundle.putString("imagePath", imagePath);
    }


    private String getOutputPath(String inputPath) {
        StringBuilder outputPathBuilder = new StringBuilder();
        String[] inputComponents = inputPath.split("\\.");

        for (int i = 0; i < inputComponents.length - 1; i++) {
            outputPathBuilder.append(inputComponents[i]);
        }
        outputPathBuilder.append("_manga.jpg");

        String outputPath;
        outputPath = outputPathBuilder.toString();

        return outputPath;
    }

}