package com.slarti.animera;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.*;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.slarti.animera.jni.MangaCreator;

import java.io.File;

public class EditMangaActivity extends ActionBarActivity {
    public static final String TAG = "EditMangaActivity";

    private static float CAPTION_TEXT_SIZE = 18;
    private static int CAPTION_TEXT_COLOR = Color.BLACK;

    private static String PATH_TYPEFACE_COMIC = "fonts/animeace2/animeace2_reg.ttf";

    private String imagePath;
    private String outPath;
    private String mCaption;

    private TextView mCaptionView;
    private ProgressDialog mangaProgress;

    private int mOrientation;
    private boolean mFromGallery = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_manga);

        Intent intent = getIntent();

        // Get image path
        imagePath = intent.getStringExtra("imagePath");

        // Get orientation
        mOrientation = intent.getIntExtra("orientation",0);

        // Form output image path
        outPath = intent.getStringExtra("mangaPath");

        mFromGallery = intent.getBooleanExtra("fromGallery",false);

        createManga();
    }

    // Show the image and a text field in the layout so the user can add a caption
    // Limit size of caption right now to about 140 characters
    private void setupEditor(Intent intent) {
        // Create bitmap and add to image view with sampling factor of 2
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 2;
        Bitmap bmp = BitmapFactory.decodeFile(outPath,opts);

        ImageView editImageView = (ImageView)findViewById(R.id.manga_preview);
        editImageView.setImageBitmap(bmp);
        editImageView.setVisibility(View.VISIBLE);
        editImageView.invalidate();

        mCaptionView = (TextView)findViewById(R.id.manga_caption);
        mCaptionView.setDrawingCacheEnabled(true);

    }

    public void createManga() {
        new MangaCreateTask().execute();
    }

    public void addCaption() {
        EditText captionTextView = (EditText)findViewById(R.id.manga_caption);
        mCaption = captionTextView.getText().toString();
        new AddCaptionTask().execute();
    }

    private class MangaCreateTask extends AsyncTask<Void,Void,Long> {

        @Override
        protected void onPreExecute(){
            // Get textview bitmap
            mangaProgress = ProgressDialog.show(EditMangaActivity.this,
                                                getString(R.string.mangaProgressTitle),
                                                getString(R.string.mangaProgressText));
        }

        @Override
        protected Long doInBackground(Void... params){
            // Check if necessary sketches exist in the sdcard
            File animeraDir = new File(Environment.getExternalStorageDirectory().getPath(),"/animera");
            File templateDir = new File(animeraDir,"/templates");
            File crossHatch = new File(templateDir,"crosshatch.jpg");

            if (!animeraDir.exists()) {
                animeraDir.mkdir();
            }

            if (!templateDir.exists()){
                templateDir.mkdir();
            }

            if (!crossHatch.exists()) {
                // Load template images
                MangaCreator.loadAssets(getAssets());
            }
            // Create actual manga
            MangaCreator.createManga(imagePath, outPath,mOrientation);

            // Delete raw image if the image was captured by the user just now
            if (!mFromGallery) {
                File sourceFile = new File(imagePath);
                if (sourceFile.exists()){
                    sourceFile.delete();
                }
            }

            return 0L;
        }

        @Override
        protected void onPostExecute(Long result){
            mangaProgress.hide();
            onMangaCreated();
        }
    }

    private class AddCaptionTask extends AsyncTask<Void,Void,Long> {

        private Bitmap mCaptionBitmap;

        @Override
        protected void onPreExecute(){
            // Get textview bitmap
            mCaptionBitmap = convertTextToImage(mCaption);
            mangaProgress = ProgressDialog.show(EditMangaActivity.this,
                    getString(R.string.mangaProgressTitle),
                    getString(R.string.mangaProgressText));
        }

        @Override
        protected Long doInBackground(Void... params){
            // Add caption
            MangaCreator.addCaption(outPath, mCaptionBitmap);
            return 0L;
        }

        @Override
        protected void onPostExecute(Long result){
            mangaProgress.hide();
            onCaptionAdded();
        }
    }


    private void onMangaCreated(){

        // Run media scanner
        MediaScannerConnection.scanFile(this,
                                        new String[] {outPath},
                                        null,
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            public void onScanCompleted(String path, Uri uri) {
                                            }});

        setupEditor(getIntent());

    }

    private void onCaptionAdded() {
        // Create intent to show image i default view
        Intent showIntent = new Intent();
        showIntent.setAction(android.content.Intent.ACTION_VIEW);
        Uri showUri = Uri.parse("file://" + outPath);
        showIntent.setDataAndType(showUri,"image/*");
        startActivity(showIntent);
        finish();

        // Finish this activity
    }

    private Bitmap convertTextToImage(String text){
        // Create Paint to draw the text
        Paint paint = new Paint();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        float scaledPixelSize = CAPTION_TEXT_SIZE;
        int pixelSize = (int)(scaledPixelSize * dm.scaledDensity);

        paint.setTextSize(pixelSize);
        paint.setColor(CAPTION_TEXT_COLOR);
        paint.setTextAlign(Paint.Align.CENTER);

        // Set typeface
        Typeface animeFont = Typeface.createFromAsset(getAssets(),PATH_TYPEFACE_COMIC);
        paint.setTypeface(animeFont);

        // Create bitmap image
        float baseline = (int) (Math.abs(paint.ascent()) + 25);
        int height = (int) (baseline + paint.descent() + 25);
        int width = (int) (paint.measureText(text) + 100); // round

        // Create canvas to draw in
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawColor(Color.WHITE);
        canvas.drawText(text, width/2, baseline, paint);

        return image;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_manga_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.edit_actions_create:
                addCaption();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}