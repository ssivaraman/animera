//
//  main.cpp
//  animera
//
//  Created by Siddharth Sivaraman on 15/06/14.
//  Copyright (c) 2014 slarti. All rights reserved.
//

#include <iostream>
#include "sketch.h"

int main(int argc, const char * argv[])
{

    Mat input = imread("../../../../../data/real_test3.jpg");
    
    printf("input cols %d",input.cols);
    
//    Mat sketch = sketchImage(input, -90);
    Mat sketch = sketchImage(input, 0);
    
//    imshow("win", sketch);
    imwrite("../../../../../data/sketch.jpg", sketch);
//    waitKey(0);

    return 0;
}

