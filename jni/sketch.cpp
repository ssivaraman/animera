#include "sketch.h"

#if !_WIN32 && !__APPLE__

#include <android/log.h>

#define  DebugLog(...)  __android_log_print(ANDROID_LOG_INFO,"MangaCreator",__VA_ARGS__)

#else

#define  DebugLog(...)  printf(__VA_ARGS__)

#endif

const int CAPTION_EDGE_THRESHOLD = 20;
const int MAX_INPUT_WIDTH = 1280;
const int CAPTION_PADDING_ROWS = 50;

const string TEMPLATE_BASE_PATH = "/mnt/sdcard/animera/templates/";

void openMat(Mat& m, Mat structure) {
	//Erode mat
	cv::erode(m,m,structure);
	// Then dilate mat
	cv::dilate(m,m,structure);
}

void closeMat(Mat& m, Mat structure) {
	//Erode mat	
	cv::dilate(m,m,structure);
	// Then dilate mat	
	cv::erode(m,m,structure);
}

Mat preprocessImageForEdges(Mat m){
	// Create structuring element
	Mat structuringElement = cv::getStructuringElement(cv::MORPH_ERODE,cv::Size(3,3),cv::Point(1,1));
	// Perform closing
	Mat mresult = Mat(m);
	closeMat(mresult,structuringElement);	
	// Perform opening on result
	openMat(mresult,structuringElement);	
	// Perform closing again on result
	closeMat(mresult,structuringElement);	
	
	return mresult;
}

// Baed on paper to enhance edges http://www.ijcsi.org/papers/7-6-187-190.pdf
Mat getEdges(Mat m) {
	Mat merode = Mat(m.rows,m.cols,m.type());
	Mat mdilate = Mat(m.rows,m.cols,m.type());
	Mat structuringElement = cv::getStructuringElement(cv::MORPH_ERODE,cv::Size(3,3),cv::Point(1,1));
	// Erode m
	cv::erode(m,merode,structuringElement);	
	// Dilate m
	cv::dilate(m,mdilate,structuringElement);	

	// Get difference and return
	Mat mresult = mdilate - merode;

	return mresult;
}

// Get laplacian edges
Mat getLaplacianEdges(Mat m) {

    cv::GaussianBlur(m, m, cv::Size(5,5), 1);
    cv::medianBlur(m, m, 7);

    Mat tempResult = Mat(m.rows,m.cols,m.type());
    cv::bilateralFilter(m, tempResult, 9, 9, 7);
    
    // Apply laplacian filter to input
    Mat mresult = Mat(m.rows,m.cols,m.type());
    Laplacian(tempResult, mresult, CV_8U,5);
    
    // Apply binary threshold
    threshold(mresult, mresult, 80, 255, THRESH_BINARY);
    
	return mresult;
}

void sketchFace (Mat& sketch,Mat original,vector<cv::Point2d> seedPoints) {
    
    
    Mat output = Mat(original.rows,original.cols,original.type());
//    input.copyTo(output);
    cvtColor(original, output, CV_RGB2YCrCb);
    
    Mat originalGray = Mat(original.rows,original.cols,CV_8UC1);
    cvtColor(original, originalGray, CV_RGB2GRAY);
    Mat edges = getLaplacianEdges(originalGray);
    
    Mat edgesPlusMask = Mat::zeros(edges.rows+2,edges.cols+2,edges.type());
    edges.copyTo(edgesPlusMask(Rect(1,1,edges.cols,edges.rows)));
    
    for (int i = 0; i < seedPoints.size();++i) {
        
        Point2d seed = seedPoints[i];
        
        cv::floodFill(output, edgesPlusMask, seed, cv::Scalar(),NULL,
                      cv::Scalar(3,1,1),
                      cv::Scalar(3,1,1),FLOODFILL_MASK_ONLY);
    }

    
    Mat edgeWithoutBorder = edgesPlusMask(Rect(1,1,edgesPlusMask.cols-2,edgesPlusMask.rows-2));
    edgeWithoutBorder -= edges;
    
    cvtColor(output, output, CV_YCrCb2RGB);
    
    Mat whites = Mat(output.rows,output.cols,output.type());
    whites = cv::Scalar(255,255,255);
    
    whites.copyTo(output,edgeWithoutBorder);
    
    Mat outputGray = Mat(output.rows,output.cols,CV_8UC1);
    cvtColor(output, outputGray, CV_RGB2GRAY);
    
    cv::medianBlur(outputGray, outputGray, 7);
    cv::medianBlur(outputGray, outputGray, 7);
    
    outputGray.copyTo(sketch,edgeWithoutBorder);
    
    return;
    
}

Mat rotateMat(Mat input, int orientation){
	Mat result;
	Mat fullRotatedMat = Mat(max(input.cols,input.rows),max(input.cols,input.rows),input.type());
	Mat fullInputMat = Mat(fullRotatedMat.rows,fullRotatedMat.cols,input.type());
	Rect inputRect = Rect((fullInputMat.cols - input.cols)/2,
							(fullInputMat.rows - input.rows)/2,
							input.cols,input.rows);
	Mat inputRectMat = fullInputMat(inputRect);
	input.copyTo(inputRectMat);
	
	Mat rotmat = cv::getRotationMatrix2D(cv::Point2f(fullRotatedMat.cols/2,fullRotatedMat.rows/2),
											-1*orientation,1);	
	cv::warpAffine(fullInputMat,fullRotatedMat,rotmat,cv::Size(fullRotatedMat.cols,fullRotatedMat.rows));
		
	Rect partRotatedRect = Rect((fullRotatedMat.cols - input.rows)/2,
								(fullRotatedMat.rows - input.cols)/2,
								input.rows,
								input.cols);
	Mat partRotatedMat = fullRotatedMat(partRotatedRect);
	
	result = Mat(input.cols,input.rows,input.type());
	partRotatedMat.copyTo(result);		
	return result;
}

vector<Rect> detectFace (Mat input) {
    Mat grayInput = Mat(input.rows,input.cols,CV_8UC1);
    cv::cvtColor(input, grayInput, CV_RGB2GRAY);
    
    string faceClassifierXml;
#if __APPLE__
    faceClassifierXml = "../../../../../data/haarcascade_frontalface_default.xml";
#else
    faceClassifierXml = TEMPLATE_BASE_PATH + "haarcascade_frontalface_default.xml";
#endif
    
    cv::CascadeClassifier faceClassifier;
    bool isLoaded = faceClassifier.load(faceClassifierXml);
    
    vector<Rect> detectedRois;
    
    if (!isLoaded) {
        return detectedRois;
    }
    
    equalizeHist(grayInput, grayInput);
    

    faceClassifier.detectMultiScale(grayInput, detectedRois);
    
    return detectedRois;
}

vector <Rect> detectEyes(Mat grayInput) {
    std::vector<Rect> eyeVector;
    string eyeClassifierXml;
#if __APPLE__
    eyeClassifierXml = "../../../../../data/haarcascade_eye_tree_eyeglasses.xml";
#else
    eyeClassifierXml = TEMPLATE_BASE_PATH + "haarcascade_eye_tree_eyeglasses.xml";
#endif
    cv::CascadeClassifier eyeClassifier;
    bool isLoaded = eyeClassifier.load(eyeClassifierXml);
    
    if (!isLoaded) {
        return eyeVector;
    }
    
    equalizeHist(grayInput, grayInput);
    
    eyeClassifier.detectMultiScale(grayInput, eyeVector);
    
    return eyeVector;
}

Mat sketchImage(Mat input, int orientation) {

	Mat m;
    Mat resizedInput;
    
    // Resize input image so that cols are MAX_INPUT_WIDTH pixels
    if (input.cols > MAX_INPUT_WIDTH) {
        float inputAspectRatio = (float)input.cols/(float)input.rows;
        int newCols = MAX_INPUT_WIDTH;
        int newRows = newCols/inputAspectRatio;
        
        resizedInput = Mat(newRows,newCols,input.type());
        cv::resize(input,resizedInput,cv::Size(resizedInput.cols,resizedInput.rows));
        
    } else {
        resizedInput = input;
    }
    
    DebugLog("Image sketched");
    
	// Rotate matrix
	if (orientation != 0) {		
		m = rotateMat(resizedInput,orientation);
	} else {
		m = resizedInput;
	}

	uchar* grayptr,*threshptr, *diffptr;

	Mat mgray = Mat(m.rows,m.cols,CV_8UC1);
	cv::cvtColor(m,mgray,CV_RGB2GRAY);

	cv::GaussianBlur(mgray,mgray,cv::Size(3,3),1);

	// Get edges
	Mat mInput = preprocessImageForEdges(mgray);
	
    Mat mdiff = getEdges(mgray);
    
	// Ensure that the edges are dark
	mdiff = 255 - mdiff;
    
    cv::threshold(mdiff, mdiff, 250, 255, CV_8UC1);

	Mat mthresh = Mat(mgray.rows,mgray.cols,mgray.type());
    
    DebugLog("Threshold mat created");
    

#if !_WIN32 && !__APPLE__
	Mat crosshatch = imread("/mnt/sdcard/animera/templates/five-points.jpg",0);
	Mat dlines = imread("/mnt/sdcard/animera/templates/dlines.jpg",0);
	Mat crosshatch2 = imread("/mnt/sdcard/animera/templates/crosshatch2.jpg",0);
#elif __APPLE__
    Mat crosshatch = imread("../../../../../data/five-points.jpg",0);
	Mat dlines = imread("../../../../../data/dlines.jpg",0);
	Mat crosshatch2 = imread("../../../../../data/crosshatch2.jpg",0);
#else
	Mat crosshatch = imread("five-points.jpg",0);
	Mat dlines = imread("dlines.jpg",0);
	Mat crosshatch2 = imread("crosshatch2.jpg",0);
	
#endif
    
    DebugLog("Loaded animera template images");
    
    if (crosshatch.data == NULL){
        DebugLog("Crosshatch data is null");
    }
    
	Mat mcrosshatch = Mat(mgray.rows,mgray.cols,mgray.type());
	Mat mdlines= Mat(mgray.rows,mgray.cols,mgray.type());
	Mat mcrosshatch2 = Mat(mgray.rows,mgray.cols,mgray.type());
	cv::resize(dlines,mdlines,cv::Size(mdlines.cols,mdlines.rows));
	cv::resize(crosshatch,mcrosshatch,cv::Size(mdlines.cols,mdlines.rows));
	cv::resize(crosshatch2,mcrosshatch2,cv::Size(mdlines.cols,mdlines.rows));

	uchar* patptr, *patptr2, *patptr3;

	for (int i = 0; i < mgray.rows;i++) {
		grayptr = (uchar *)(mgray.data + mgray.step*i);
		diffptr = (uchar *)(mdiff.data + mdiff.step*i);
		patptr = (uchar *)(mdlines.data + mdlines.step*i);
		patptr2 = (uchar *)(mcrosshatch2.data + mcrosshatch2.step*i);
		patptr3 = (uchar *)(mcrosshatch.data + mcrosshatch.step*i);
		threshptr = (uchar *)(mthresh.data + mthresh.step*i);
		for (int j = 0; j < mgray.cols;j++) {
			int thresholdVal = ((int)grayptr[j]/25)*25;						
			if (thresholdVal >= 70) {
				thresholdVal = 255;
			} else if (thresholdVal <= 20) {
				thresholdVal = 0;
			} else if (thresholdVal >= 50) {
				thresholdVal = patptr[j];				
			} else if (thresholdVal >= 30) {
				thresholdVal = patptr2[j];
			} else if (thresholdVal >= 20) {
				thresholdVal = patptr3[j];
			}
			if (diffptr[j] != 255) {
				threshptr[j] = diffptr[j];
			} else {
				threshptr[j] = thresholdVal;
			}

		}
	}
    
	return mthresh;
}

void addCaption(Mat& sketchedMat,Mat captionMat) {
    // Embed caption inside image
    float aspectRatio = (float)captionMat.cols/captionMat.rows;
    
	// Convert RGB caption to gray
	Mat captionMatGray = Mat(captionMat.rows,captionMat.cols,sketchedMat.type());
	if (captionMat.channels() == 3) {
		cvtColor(captionMat,captionMatGray,CV_RGB2GRAY);
	} else {
		captionMatGray = captionMat;
	}
	
	// Manually add the caption inside the sketchedMat image
	int xoffset = (sketchedMat.cols - captionMat.cols)/2;
	int yoffset = sketchedMat.rows - captionMat.rows - CAPTION_PADDING_ROWS;
	uchar* captionPtr, *sketchedPtr;
	float alpha;
	int edgeThreshold = CAPTION_EDGE_THRESHOLD;
	
	for (int i = 0; i < captionMatGray.rows; i++) {
		captionPtr = (uchar *)(captionMatGray.data  + captionMatGray.step*i);
		sketchedPtr = (uchar *)(sketchedMat.data  + sketchedMat.step*(i + yoffset));
		for (int j = 0; j < captionMatGray.cols; j++) {
			int distFromEdge = min(captionMatGray.cols/2 - abs(j - captionMatGray.cols/2),
								   captionMatGray.rows/2 - abs(i - captionMatGray.rows/2));
			if (distFromEdge > edgeThreshold) {
				alpha = 1;
			} else {
				alpha = (float)distFromEdge/(float)edgeThreshold;
			}
			sketchedPtr[j + xoffset] = alpha*captionPtr[j] +
            (1 - alpha)*sketchedPtr[j + xoffset];
		}
	}
}

#if _WIN32
void main(int argc, char** argv) {
	Mat input = imread("test3.jpg");	
	Mat caption = imread("caption.jpg");

	Mat result = sketchImage(input,caption,270);
	imwrite("result.jpg",result);
}
#endif