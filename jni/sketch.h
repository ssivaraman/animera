#include <stdlib.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

using namespace cv;

Mat sketchImage(Mat m, int orientation);

void addCaption(Mat& sketchedMat,Mat captionMat);