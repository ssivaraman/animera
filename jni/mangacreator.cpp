#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <jni.h>
#include <android/log.h>
#include <android/bitmap.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <sketch.h>

using namespace cv;
using namespace std;

#define LOG_TAG "MangaCreator"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

extern "C" {


void writeAssetToSDCard(AAssetManager* mgr, const char* filename){
	
	   LOGI("Assets inside templates %s",filename);
	   // Get the asset
	   AAsset* asset = AAssetManager_open(mgr, filename, AASSET_MODE_STREAMING);

	   if (asset == NULL){
	   	LOGI("NUll asset");
	   }
	   
	   // Get the destination filename where it will be stored
	   string destinationFilename("/mnt/sdcard/animera/");
	   destinationFilename += filename;
	   	   
	   LOGI("destination filename %s",destinationFilename.c_str());

	   // Copy from asset to the destination
	   char buf[10000];
	   int nb_read = 0;
	   FILE* out = fopen(destinationFilename.c_str(), "w");
	   LOGI("Before start read");
	   while ((nb_read = AAsset_read(asset, buf, 10000)) > 0) {
	   		LOGI("reading %d bytes",nb_read);
	       fwrite(buf, nb_read, 1, out);
	   }
	   fclose(out);

	   // Close asset
	   AAsset_close(asset);
}

Mat bitmapToMat(JNIEnv* env, jobject bitmap) {
	// Convert the bitmap to a Mat
	AndroidBitmapInfo* info = new AndroidBitmapInfo();	
	uchar* pix_data;	

	LOGI("Inside bitmapToMat");

	AndroidBitmap_getInfo(env,bitmap,info);
	const int width = info->width;
	const int height = info->height;

	LOGI("Android bitmap width %d height %d",info->width,info->height);	

	// Lock pixels so that you can use them safely in the native side
	AndroidBitmap_lockPixels(env,bitmap,(void**)&pix_data);

	// use bitmap pixels to create a mat.
	Mat captionBitmapMat = Mat(height,width,CV_8UC4,pix_data);
	Mat captionMatRGBA = Mat(height,width,CV_8UC4);
	captionBitmapMat.copyTo(captionMatRGBA);

	AndroidBitmap_unlockPixels(env,bitmap);

	LOGI("Mat from bitmap created");

	// Convert png to jpeg
	int fromTo[] = {0,0,1,1,2,2};
	Mat captionMatRGB = Mat::zeros(height,width,CV_8UC3);
	mixChannels(&captionMatRGBA,1,&captionMatRGB,1,fromTo,3);

	LOGI("Mat saved");

	return captionMatRGB;
}

void Java_com_slarti_animera_jni_MangaCreator_createManga(JNIEnv* env, jobject thiz, 
														  jstring jpath, jstring joutpath,
														  jint orientation) {
	const char* cpath = env->GetStringUTFChars(jpath, NULL);
	const char* coutpath = env->GetStringUTFChars(joutpath, NULL);
	
	if (cpath == NULL) {
		return;
	}
	
	string strpath(cpath);	
	string stroutpath(coutpath);
		
	Mat input = imread(strpath);
    
    LOGI("Before create manga");
	Mat output = sketchImage(input,orientation);
    
	imwrite(stroutpath,output);	
}

void Java_com_slarti_animera_jni_MangaCreator_loadAssets(JNIEnv* env, jobject thiz, 
														 jobject assetManager){
 	LOGI("Load assets");
 	AAssetManager* asset_manager = AAssetManager_fromJava(env,assetManager);

 	AAssetDir* assetDir = AAssetManager_openDir(asset_manager, "templates");
	const char* filename;
	
	while ((filename = AAssetDir_getNextFileName(assetDir)) != NULL)
	{
		string fullFilename("templates/");
        LOGI("Loading asset %s",fullFilename.c_str());
		fullFilename += filename;
		writeAssetToSDCard(asset_manager,fullFilename.c_str());
	}
	AAssetDir_close(assetDir);
    LOGI("Assets loaded successfully");
}

void Java_com_slarti_animera_jni_MangaCreator_addCaption(JNIEnv* env, jobject thiz,
                                                          jstring imagePath, jobject captionBitmap) {
    
    //
    
    const char* cImagePath = env->GetStringUTFChars(imagePath, NULL);
    
    if (cImagePath == NULL) {
        return;
    }
    
    string strImagePath(cImagePath);
    
    Mat input = imread(strImagePath,0);
    Mat captionMat = bitmapToMat(env,captionBitmap);
    
    LOGI("caption mat size %d x %d",captionMat.cols,captionMat.rows);
    LOGI("input mat size %d x %d",input.cols,input.rows);

    addCaption(input,captionMat);
    
    imwrite(strImagePath,input);
    //imwrite(strImagePath,captionMat);
}
   
    
}